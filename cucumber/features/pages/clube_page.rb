# encoding: utf-8

require 'site_prism'

module DZWebSite
  module Pages
    class ClubePage < SitePrism::Page
      set_url '/dashboard/clubedotz/extrato'
      element :box_status, '#boxStatusClube'
    end
  end
end