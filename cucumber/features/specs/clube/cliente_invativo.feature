# language:pt

@clube
Funcionalidade: Clube Dotz cliente invativo
  Sendo um cliente invativo
  Posso acessar a página do Clube Dotz
  Para que possa ver os detalhes da minha conta no Clube

  Contexto:
    Dado que minha assinatura esta "Cancelada"
    Quando faço login no website
    Entao vejo o dashboard com os detalhes da minha conta

  Cenário: Assinatura com status cancelada
    Quando acesso a página do Clube Dotz
    Entao vejo da minha conta com status "Cancelada" com a seguinte mensagem
    """
    Sua assinatura foi finalizada. Clique aqui, realize um novo pagamento e volte a aproveitar as promoções exclusivas!
    """
