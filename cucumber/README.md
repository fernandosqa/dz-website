# Cucumber DZWebSite

Getting Started
===============

### Installation

* Install Ruby 2.3.1 from https://www.ruby-lang.org/en/downloads/ or via RVM (https://rvm.io/) or Windows (https://rubyinstaller.org/)
* Install `.Ruby >2.3.1`
* Install `Firefox`
* Install `Chrome`
* Install `Bundler`
* Install `Nodejs`
* sudo npm install `geckodriver` -g
* sudo npm install `chromedriver` -g
* sudo npm install `phantomjs` -g

### Windows only

* Install devkit from http://rubyinstaller.org/downloads/
  
### Basic Guide

* Run tests with `poltergeist`
```    
bundle exec cucumber -p poltergeist
```
* Run tests with `firefox`
```
bundle exec cucumber -p firefox
```
* Run tests with `chrome`
```
bundle exec cucumber -p chrome
```
* HTML Report
```
bundle exec cucumber -p html_report
```
* JSON Report for CI
```
bundle exec cucumber -p json_report
```
