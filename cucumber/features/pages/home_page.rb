require 'site_prism'

module DZWebSite
  module Sections
    class Modal < SitePrism::Section
      element :close, '.close'
    end
    class LoginBox < SitePrism::Section
      element :id, 'input[id$=txtLogin]'
      element :sign_in, '.login-btn'
    end
    class NavUser < SitePrism::Section
      element :balance, '#spanSaldo'
      element :name, '.nav-user-name'
    end
  end
  module Pages
    class HomePage < SitePrism::Page
      set_url '/'
      section :modal, DZWebSite::Sections::Modal, '.modal-open'
      section :login_box, DZWebSite::Sections::LoginBox, '.login-cta'
      section :nav_user, DZWebSite::Sections::NavUser, '.nav-user'

      def get_balance
        balance = self.nav_user.balance.text.gsub! 'DZ', ''
        balance.gsub! '.', ''
        return balance.to_i
      end
    end
  end
end


