# language:pt

@login
Funcionalidade: Login
  Sendo um cliente Dotz
  Posso fazer login no website
  Para que eu possa ver meu saldo, realizar resgates e visualizar minha conta

  Cenário: Login com cliente Varejo
    Dado que sou cliente "Varejo"
    Quando faço login no website
    Entao vejo meu saldo em pontos Dotz

  Cenário: Login com cliente Banco do Brasil
    Dado que sou cliente "BB"
    Quando faço login no website
    Entao vejo meu saldo em pontos Dotz
