
Dir[File.join(File.dirname(__FILE__), '../pages/*.rb')].each { |file| require file }

module DZWebSite
  module Pages
    def home
      DZWebSite::Pages::HomePage.new
    end
    def sso
      DZWebSite::Pages::SSOPage.new
    end
    def clube
      DZWebSite::Pages::ClubePage.new
    end
  end
end
