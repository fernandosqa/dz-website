# encoding: utf-8

Dado(/^que sou cliente "([^"]*)"$/) do |client|
  @user = '31176361880'
  @pwd = '180208'
  @name = 'Fernando'
  @birth_date = '18-02-83'
end

Quando(/^faço login no website$/) do
  home.login_box.id.set @user
  home.login_box.sign_in.click
  sso.next_button.text.should have_content @name
  sso.login_with(@pwd, @birth_date)
  home.nav_user.name.should have_content @name
end

Entao(/^vejo meu saldo em pontos Dotz$/) do
  balance = home.get_balance 
  balance.should be > 0
end