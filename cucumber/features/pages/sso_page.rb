# encoding: utf-8

require 'site_prism'

module DZWebSite
  module Pages
    class SSOPage < SitePrism::Page
      element :next_button, '#btn-next'
      element :confirm_button, '#btn-confirm'
      element :question_date, '#question-date'
      element :answer_date, '#data-answer-date'
      element :question_date_disabled, '.perg-secreta input[disabled=disabled]'

      def type_password(pwd)
        pwd.split('').each do |i|
          find('.btn-usersenha', :text => i).click
        end
      end

      def type_date(value)
        question = self.question_date.text
        case question
          when 'Digite no campo abaixo o dia do seu nascimento:'
            self.answer_date.set value.split("-")[0]
          when 'Digite no campo abaixo o mês do seu nascimento:'
            self.answer_date.set value.split("-")[1]
          when 'Digite no campo abaixo os dois últimos dígitos do ano do seu nascimento:'
            self.answer_date.set value.split("-")[2]
        end
      end

      def login_with (pwd, birth_date)
        self.next_button.click
        self.type_password(pwd)
        self.has_no_question_date_disabled?
        self.type_date(birth_date)
        self.confirm_button.click
      end

    end
  end
end